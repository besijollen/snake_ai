# README #

### Snake AI and Neulib ###

Neulib is a framework for creating neural networks and training them. Here it is implemented in a self-learning game of Snake.

### Setup ###

Pip install these libraries:

* pygame
* pickle

Execute `python3 SnakeNest.py <MODE>` to run and start spawning snakes.

###### Training mode ######
`<MODE> = train`

This will:

* configure a random network (or use the `saved_models/best_models.txt` file if it exists)
* play games of Snake using this network and occasionally make random moves
* adjust the network based on the result after each move
* save a new `saved_models/best_models.txt` file after each round

At the moment it typically takes around 40000 rounds to learn how to play. The next chellenge is to decrease this.

###### Play mode ######
`<MODE> = play`

This will do the same as the training mode, except:

* it will not make any random moves
* it will display the games using pygame