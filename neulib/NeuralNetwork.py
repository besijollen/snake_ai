import numpy as np
import math

class NeuralNetwork(object):
    def __init__(self, num_inputs, hidden_layer_structure, num_outputs, activation='sigmoid'):
        layer_structure = [num_inputs, *hidden_layer_structure, num_outputs]
        self.weights, self.biases = self.initialize_random_network(layer_structure)
        self.layer_structure = layer_structure
        self.activation = activation
        return

    def initialize_random_network(self, layer_structure):
        weights = []
        biases = []
        previous_layer_size = layer_structure[0]
        for layer_size in layer_structure[1:]:
            weights.append(np.random.randn(previous_layer_size, layer_size) * 0.01)
            biases.append(np.random.randn(layer_size) * 0.01)
            previous_layer_size = layer_size
        return weights, biases

    def activation_function(self, value):
        if self.activation == 'sigmoid':
            return 1 / (1 + math.exp(-value))
        if self.activation == 'relu':
            return max(0, value)
        raise Exception("Unrecognized activation function: " + self.activation)

    def compute_output(self, inputs):
        previous_layer = inputs
        for weights, biases in zip(self.weights, self.biases):
            next_layer = np.matmul(previous_layer, weights) + biases
            next_layer = [self.activation_function(element) for element in next_layer]
            previous_layer = next_layer
        return np.array(previous_layer)

    def get_layer_outputs(self, inputs):
        layer_outputs = []
        layer_outputs.append(np.array(inputs))
        previous_layer = inputs
        for weights, biases in zip(self.weights, self.biases):
            next_layer = np.matmul(previous_layer, weights) + biases
            next_layer = np.array(list(map(self.activation_function, next_layer)))
            layer_outputs.append(next_layer)
            previous_layer = next_layer
        return layer_outputs

    def backpropagate_weight_errors(self, errors):
        previous_layer = errors
        output = [previous_layer]
        for weights in reversed(self.weights):
            next_layer = np.matmul(weights, previous_layer)
            previous_layer = np.array(next_layer)
            output.append(previous_layer)
        output.reverse()
        return output