import numpy as np
import math
import os
import sys
from NeuralNetwork import NeuralNetwork
import copy
import matplotlib.pyplot as plt

class Trainer(object):
    def __init__(self, learning_rate, activation='sigmoid', batch_size = 100):
        self.learning_rate = learning_rate
        self.activation = activation
        self.batch_size = batch_size
        return

    def cost(self, network, inputs, targets):
        outputs = network.compute_output(inputs)
        cost = 0
        for output, target in zip(outputs, targets):
            cost += (output - target) ** 2
        return cost

    def train(self, network, X, y):
        trained_network = copy.deepcopy(network)
        for i in range(int(math.ceil(len(X)/self.batch_size))):
            trained_network = self.backprop_data_batch(trained_network, X[i*self.batch_size:(i+1)*self.batch_size], y[i*self.batch_size:(i+1)*self.batch_size])
        return trained_network

    def backprop_data_batch(self, network, X, y):
        for data, label in zip(X, y):
            self.backprop_datapoint(network, data, label)
        return network

    def activation_differential(self, x):
        if self.activation == 'sigmoid':
            return x * (1 - x)
        if self.activation == 'relu':
            return 1 if x > 0 else 0
        raise Exception("Unrecognized activation function: " + self.activation)

    def backprop_datapoint(self, network, data, label):
        layer_outputs = network.get_layer_outputs(data)
        output_error = np.subtract(label, layer_outputs[-1]) # index -1 points to last value in list
        backpropagated_errors = network.backpropagate_weight_errors(output_error)

        for layer in range(len(network.layer_structure) - 1):
            layer_a = layer
            layer_b = layer + 1
            layer_a_shape = network.layer_structure[layer_a]
            layer_b_shape = network.layer_structure[layer_b]

            layer_b_error = backpropagated_errors[layer_b].reshape(1, layer_b_shape)
            layer_b_differential = np.array(list(map(self.activation_differential, layer_outputs[layer_b]))).reshape(1, layer_b_shape)
            layer_a_transpose = np.array(layer_outputs[layer_a]).reshape(layer_a_shape, 1)

            gradient_weights = self.learning_rate * layer_b_error * layer_b_differential * layer_a_transpose
            network.weights[layer_a] += gradient_weights

            gradient_biases = self.learning_rate * layer_b_error * layer_b_differential
            network.biases[layer_a] += gradient_biases.reshape(layer_b_shape)