from enum import Enum
import pygame
import numpy as np

#possible states for a square: empty, food, head, body (0, 1, 2, 3)
#possible outputs: nothing, up, down, left, right (0, 1, 2, 3, 4)

class Actions(Enum):
    up = 0
    down = 1
    left = 2
    right = 3

    def get_pygame_key(self):
        if self == Actions.up:
            return pygame.K_UP
        if self == Actions.down:
            return pygame.K_DOWN
        if self == Actions.left:
            return pygame.K_LEFT
        if self == Actions.right:
            return pygame.K_RIGHT
        return None

    def get_action(pygame_key):
        if pygame_key == pygame.K_UP:
            return Actions.up
        if pygame_key == pygame.K_DOWN:
            return Actions.down
        if pygame_key == pygame.K_LEFT:
            return Actions.left
        if pygame_key == pygame.K_RIGHT:
            return Actions.right
        return None


    def get_new_coord(self, current_coord):
        if self == Actions.up:
            return (current_coord[0], current_coord[1] - 1)
        if self == Actions.down:
            return (current_coord[0], current_coord[1] + 1)
        if self == Actions.left:
            return (current_coord[0] - 1, current_coord[1])
        if self == Actions.right:
            return (current_coord[0] + 1, current_coord[1])
        return current_coord
