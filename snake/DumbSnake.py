import time
import random
import pygame
from Actions import Actions

class DumbSnake(object):
    def __init__(self, size, training_run = False):
        self.training_run = training_run
        self.block_pixel_print_size = 8
        self.block_pixel_size = 10
        self.board_dimensions = (size, size)
        self.white = (255, 255, 255)
        self.black = (0, 0, 0)
        self.red = (255, 0, 0)
        self.blue = (0, 0, 255)
        self.last_action = Actions.right
        self.snake_coords = [(int(0.3 * size) - 1, int(0.4 * size)), (int(0.3 * size), int(0.4 * size))]
        self.snake_directions = [self.last_action, self.last_action]
        self.food = self.generate_food()
        self.end_game = False
        if not self.training_run:
            pygame.init()
            pygame.font.init()
            self.font = pygame.font.SysFont('Comic Sans MS', 11)
            self.dis = pygame.display.set_mode([element * self.block_pixel_size for element in self.board_dimensions])
        self.moves_since_last_food = 0
        self.move_limit = size ** 2

    def generate_random_coord(self):
        return (random.randrange(self.board_dimensions[0]),
                random.randrange(self.board_dimensions[1]))

    def generate_food(self):
        food = self.generate_random_coord()
        while (food in self.snake_coords):
            food = self.generate_random_coord()
        return food

    def perform_action(self, action):
        self.last_action = action
        snake_head_coord = self.snake_coords[-1]
        if not self.training_run:
            for event in pygame.event.get():
                if (event.type == pygame.QUIT):
                    self.end_game = True
                    self.gameover()
                if (event.type == pygame.KEYDOWN):
                    action = Actions.get_action(event.key)
                    self.last_action = action

        new_snake_head_coord = self.last_action.get_new_coord(snake_head_coord)

        if (new_snake_head_coord == self.food):
            self.snake_coords.append(new_snake_head_coord)
            self.snake_directions.append(self.last_action)
            self.food = self.generate_food()
            self.moves_since_last_food = 0
        else:
            self.snake_coords.pop(0)
            self.snake_directions.pop(0)
            self.moves_since_last_food += 1

            if (new_snake_head_coord in self.snake_coords
                or self.moves_since_last_food > self.move_limit
                or not self.board_dimensions[1] > new_snake_head_coord[1] >= 0
                or not self.board_dimensions[0] > new_snake_head_coord[0] >= 0):
                self.end_game = True
                self.gameover()
                return

            self.snake_coords.append(new_snake_head_coord)
            self.snake_directions.append(self.last_action)


        if not self.training_run:
            self.dis.fill(self.white)
            for coordinate in self.snake_coords:
                self.print_block(coordinate, self.black)
            self.print_block(self.food, self.red)
            self.print_block(new_snake_head_coord, self.blue)
            pygame.display.update()

    def print_block(self, location, colour):
         coordinates = [element * self.block_pixel_size for element in location]
         pygame.draw.rect(self.dis, colour, [*coordinates, self.block_pixel_print_size, self.block_pixel_print_size])

    def main(self):
        if not self.training_run:
            pygame.display.update()
            pygame.display.set_caption("Vanilla Snake")

    def gameover(self):
        if not self.training_run:
            gameover = self.font.render("GAME OVER!", False, self.red)
            score = self.font.render("Score: " + str(len(self.snake_coords)), False, self.red)
            self.dis.blit(gameover, (5, 10))
            self.dis.blit(score, (5, 30))
            pygame.display.update()
            time.sleep(0.2)