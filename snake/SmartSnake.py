import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "../neulib")))
from DumbSnake import DumbSnake
import math
import time
import numpy as np
from Actions import Actions
from Types import GameHistory
from NeuralNetwork import NeuralNetwork
from SnakeTrainer import SnakeTrainer
from Types import NetworkAndHighscore, GameConfig

class SmartSnake(object):
    def __init__(self, game_config, randomization_rate, network_and_highscore=None):
        self.training_run = game_config.training_run

        self.sectors = 8
        self.score = 0
        self.randomization_rate = randomization_rate
        self.history = GameHistory()
        self.trainer = SnakeTrainer()


        self.delay = 0 if self.training_run else 0.02
        self.board_size = game_config.board_size
        self.game = DumbSnake(self.board_size, self.training_run)
        self.num_inputs = len(self.get_current_state())
        self.hidden_layer_structure = game_config.hidden_layer_structure
        self.num_outputs = game_config.num_outputs

        if (network_and_highscore != None):
            self.current_high_score = network_and_highscore.highscore
            self.network = network_and_highscore.network
        else:
            self.current_high_score = 0
            self.network = NeuralNetwork(self.num_inputs, self.hidden_layer_structure, self.num_outputs)

    def get_current_state(self):
        head_coord = self.game.snake_coords[len(self.game.snake_coords) - 1]
        head_dir = self.game.snake_directions[len(self.game.snake_directions) - 1]
        ohe_head_dir = self.one_hot([head_dir.value], len(Actions))
        ohe_tail_dir = self.one_hot([head_dir.value], len(Actions))
        food_sector = self.one_hot_distance([self.get_sector_and_distance(head_coord, self.game.food)], self.sectors)
        body_sectors = self.one_hot_distance([self.get_sector_and_distance(head_coord, body_coord) for body_coord in self.game.snake_coords[:(len(self.game.snake_coords)-1)]], self.sectors)
        wall_above = 1 / (head_coord[1] + 1)
        wall_left = 1 / (head_coord[0] + 1)
        wall_below = 1 / (self.board_size - head_coord[1])
        wall_right = 1 / (self.board_size - head_coord[0])

        return np.concatenate([ohe_head_dir, ohe_tail_dir, food_sector, body_sectors, [wall_above, wall_below, wall_left, wall_right]])

    def one_hot(self, values, total):
        output = np.zeros(total)
        for i in range(total):
            if i in values:
                output[i] = 1
        return output

    def one_hot_distance(self, values, total):
        output = np.zeros(total)
        for value in values:
            if value[0] > output[value[1]]:
                output[value[1]] = value[0]
        return output

    def get_angle(self, coord1, coord2):
        diff_x = coord1[0] - coord2[0]
        diff_y = coord1[1] - coord2[1]
        return math.atan2(diff_x, diff_y)

    def get_sector_and_distance(self, coord1, coord2):
        tau = 2 * math.pi
        return self.inverse_distance_between_points(coord1, coord2), int((self.get_angle(coord1, coord2) % tau) * self.sectors / tau)

    def inverse_distance_between_points(self, coord1, coord2):
        return 1 / ((((coord1[0] - coord2[0]) ** 2) + (coord1[1] - coord2[1]) ** 2) ** 0.5)

    def main(self):
        self.game.main()
        while(not self.game.end_game):
            randomization_rate = 1 / (1 + math.exp(self.current_high_score - self.score))
            state_before_action = self.get_current_state()
            score_before_action = len(self.game.snake_coords)
            if (np.random.uniform(0, 1) < self.randomization_rate):
                action = np.random.choice(Actions)
            else:
                move = self.network.compute_output(state_before_action)
                action = Actions(np.argmax(move))
            self.game.perform_action(action)
            self.score = len(self.game.snake_coords)
            self.history.add_state(state_before_action, action, self.score - score_before_action)
            time.sleep(self.delay)
        self.trainer.train_one_game(self.network, self.history)
        highscore = max(self.current_high_score, len(self.game.snake_coords))
        return NetworkAndHighscore(self.network, highscore)

if __name__ == "__main__":
    output = SmartSnake().main()