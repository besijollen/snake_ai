import os
from SmartSnake import SmartSnake
from Types import NetworkAndHighscore, GameConfig
import time
import pickle
import sys
from Actions import Actions

class SnakeNest(object):

    def __init__(self, training_run):
        self.training_run = training_run
        self.game_config = GameConfig(hidden_layer_structure=[25], num_outputs=len(Actions), board_size=16, training_run=self.training_run)

        self.num_games_per_round = 50
        self.num_rounds_until_purge = 5000
        self.purge_proportion = 0.5
        self.total_rounds = 1000000
        self.best_ever_score = 0
        self.start_time = time.time()

        self.current_games = []
        self.best_models = []

        self.saved_model_path = "saved_models/best_models.txt"
        if (os.path.isfile(self.saved_model_path)):
            with open(self.saved_model_path, 'rb') as fp:
                self.best_models, self.round = pickle.load(fp)
            print("Found saved model. Starting from round: ", self.round)
        else:
            self.round = 0

    def setup_games(self):
        randomization_rate = 5000 / (5000 + self.round) if self.training_run else 0
        if (self.round == 0):
            self.current_games = [SmartSnake(self.game_config, randomization_rate) for _ in range(self.num_games_per_round)]
        elif (self.round % self.num_rounds_until_purge == 0 and len(self.current_games) > 0):
            num_games_to_drop = int(self.num_games_per_round * self.purge_proportion)
            num_games_to_keep = self.num_games_per_round - num_games_to_drop
            self.current_games = [SmartSnake(self.game_config, randomization_rate, model) for model in self.best_models[:num_games_to_keep]] \
                   + [SmartSnake(self.game_config, randomization_rate) for _ in range(num_games_to_drop)]
        else:
            self.current_games = [SmartSnake(self.game_config, randomization_rate, model) for model in self.best_models]

        self.best_models = []

    def play_game(self, game):
        return game.main()

    def play_games(self):
        for game in self.current_games:
            self.best_models.append(game.main())

    def find_best_game(self):
        self.best_models = sorted(self.best_models, key=lambda model: model.highscore, reverse=True)
        test_snake = SmartSnake(self.game_config, 0, self.best_models[0])
        result = test_snake.main()
        self.best_ever_score = max(self.best_ever_score, result.highscore, self.best_models[0].highscore)
        if (self.round % 25 == 0):
            print("~~~~~~~~~~~~~~~~~~~~~~~~~")
            print("Round: ", self.round)
            print("Best ever score: ", self.best_ever_score)

        with open('saved_models/best_models.txt', 'wb') as fp:
            pickle.dump([self.best_models, self.round], fp)

    def main(self):
        while (self.round < self.total_rounds):
            self.setup_games()
            self.play_games()
            self.find_best_game()
            self.round += 1

if __name__ == "__main__":
    if (len(sys.argv) == 2 and sys.argv[1] == 'train'):
        SnakeNest(training_run=True).main()
    elif (len(sys.argv) == 2 and sys.argv[1] == 'play'):
        SnakeNest(training_run=False).main()
    else:
        raise ValueError("Please run with input parameter 'train' or 'play'")