import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "../neulib")))
from Trainer import Trainer
import numpy as np

class SnakeTrainer(object):
    def __init__(self):
        self.trainer = Trainer(0.01)

    def train_one_game(self, network, history):
        for index in range(len(history.states)):
            self.train_one_state(network, history, index)

    def train_one_state(self, network, history, index):
        total_reward = np.sum(history.rewards[index:])
        move_value =  0 if total_reward < 0 else 1
        output = network.compute_output(history.states[index])
        move_index = history.actions[index].value
        label = output
        label[move_index] = move_value

        self.trainer.backprop_datapoint(network, history.states[index], label)