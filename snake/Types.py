class NetworkAndHighscore(object):
    def __init__(self, network, highscore):
        self.network = network
        self.highscore = highscore

class GameConfig(object):
    def __init__(self, hidden_layer_structure, num_outputs, board_size, training_run):
        self.hidden_layer_structure = hidden_layer_structure
        self.num_outputs = num_outputs
        self.board_size = board_size
        self.training_run = training_run

class GameHistory(object):
    def __init__(self):
        self.states = []
        self.actions = []
        self.rewards = []

    def add_state(self, state, action, reward):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)